create or replace package body lsa_blm_threshold_calcs
is
    ------------------------------------------------------------------------------------------------------------------------
--  (c) CERN 2005-2014 copyright
------------------------------------------------------------------------------------------------------------------------
--  Description:
--   This package is the entry point for calculating Beam Loss Monitor beam abort thresholds
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2014-01-15 : Marcin Sobieszek
--    Creation
------------------------------------------------------------------------------------------------------------------------

    function get_param_value_for (
        i_params                 in lsa_blm_types.t_params,
        i_param_name             in blm_threshold_parameters.parameter_name%type,
        i_is_config_value        in boolean := false,
        i_action_order           in v_stage_blm_threshold_attrs.action_order%type default null
    ) return varchar2
        ------------------------------------------------------------------------------------------------------------------------
--  Description:
--    	   This function gets the value for the given parameter
--
------------------------------------------------------------------------------------------------------------------------
--  Input Arguments:
--   i_params     - the supported configuration parameters
--   i_param_name - the parameter name
------------------------------------------------------------------
--  Returns
--    the corresponding parameter value, null if the given param is not supported
------------------------------------------------------------------------------------------------------------------------
--  Exceptions:
--    No known exceptions
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--    2014-01-20 : Marcin Sobieszek
--    Creation
------------------------------------------------------------------------------------------------------------------------
        is
    begin
        for i in 1 .. i_params.count loop
            if i_params(i).param_name = i_param_name then
                if coalesce(i_action_order, i_params(i).operation_order) != i_params(i).operation_order then
                    continue;
                end if;
                if i_is_config_value then
                    return i_params(i).param_config;
                end if;
                return i_params(i).param_value;
            end if;
        end loop;

        return null;
    end get_param_value_for;

    function get_config_value_for (
        i_params                 in lsa_blm_types.t_params,
        i_param_name             in blm_threshold_parameters.parameter_name%type
    ) return varchar2
        ------------------------------------------------------------------------------------------------------------------------
--  Description:
--    	   This function gets the configuration value for the given parameter
--
------------------------------------------------------------------------------------------------------------------------
--  Input Arguments:
--   i_params     - the supported configuration parameters
--   i_param_name - the parameter name
------------------------------------------------------------------
--  Returns
--    the corresponding configuration value, null if the given param is not supported
------------------------------------------------------------------------------------------------------------------------
--  Exceptions:
--    No known exceptions
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--    2014-01-20 : Marcin Sobieszek
--    Creation
------------------------------------------------------------------------------------------------------------------------
        is
    begin
        return get_param_value_for(i_params, i_param_name, true);
    end get_config_value_for;

    function get_param_values_for(
        i_params                 in lsa_blm_types.t_params,
        i_param_name             in blm_threshold_parameters.parameter_name%type,
        i_action_order           in v_stage_blm_threshold_attrs.action_order%type default null
    ) return v_stage_blm_threshold_attrs.vector_value%type
        ------------------------------------------------------------------------------------------------------------------------
--  Description:
--    	   This function gets the list of param values indexed by their indexes for the given parameter
--
------------------------------------------------------------------------------------------------------------------------
--  Input Arguments:
--   i_params     - the supported configuration parameters
--   i_param_name - the parameter name
------------------------------------------------------------------
--  Returns
--    the list of corresponding parameter values; an empty list if the given param is not supported
------------------------------------------------------------------------------------------------------------------------
--  Exceptions:
--    No known exceptions
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--    2014-01-20 : Marcin Sobieszek
--    Creation
------------------------------------------------------------------------------------------------------------------------
        is
        l_params                    v_stage_blm_threshold_attrs.vector_value%type := vectornumeric();
    begin
        for i in 1 .. i_params.count loop
            if i_params(i).param_name = i_param_name then
                if coalesce(i_action_order, i_params(i).operation_order) != i_params(i).operation_order then
                    continue;
                end if;
                return i_params(i).param_values;
            end if;
        end loop;

        return l_params;
    end get_param_values_for;

    function is_param_supported(
        i_params                 in lsa_blm_types.t_params,
        i_param_name             in blm_threshold_parameters.parameter_name%type
    ) return boolean
        ------------------------------------------------------------------------------------------------------------------------
--  Description:
--    	   The function checks if the given configuration parameter is supported
--
------------------------------------------------------------------------------------------------------------------------
--  Input Arguments:
--   i_params     - the supported configuration parameters
--   i_param_name - the parameter name
------------------------------------------------------------------
--  Returns
--    true if the given parameter is supported
------------------------------------------------------------------------------------------------------------------------
--  Exceptions:
--    No known exceptions
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--    2014-01-20 : Marcin Sobieszek
--    Creation
------------------------------------------------------------------------------------------------------------------------
        is
        l_params                    lsa_blm_types.t_params;
    begin
        return get_param_value_for(i_params, i_param_name, true) is not null
            or get_param_value_for(i_params, i_param_name, false) is not null
            or get_param_values_for(i_params, i_param_name).count > 0;
    end is_param_supported;

    procedure validate_params(
        i_params              in lsa_blm_types.t_params,
        i_element_type        in blm_protected_elem_types.element_type%type
    )
        ------------------------------------------------------------------------------------------------------------------------
--  Description:
--    	   This procedure checks if the list of supported configuration parameters contains the required parameters
--         for the given method name
--
------------------------------------------------------------------------------------------------------------------------
--  Input Arguments:
--   i_params      - the supported configuration parameters
--   i_method_name - the method name
------------------------------------------------------------------------------------------------------------------------
--  Exceptions:
--    Raises commons error event BLM_THRESH_REQ_PARAM_MISSING when a parameter is required for the given element type
--    but not present in the configuration.
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--    2014-01-20 : Marcin Sobieszek
--    Creation
------------------------------------------------------------------------------------------------------------------------
        is

    begin
        for r_param in (
            select p.parameter_name
            from blm_threshold_parameters p
                     join blm_protected_elem_type_params pe on (p.parameter_id = pe.parameter_id)
            where pe.element_type = i_element_type
            ) loop
            if not is_param_supported(i_params, r_param.parameter_name) then
                com_event_mgr.raise_error_event('BLM_THRESH_REQ_PARAM_MISSING', 'Parameter [' || r_param.parameter_name || '] is required for the element type = '
                    || i_element_type||dbms_utility.format_error_stack);
            end if;
        end loop;
    exception
        when NO_DATA_FOUND THEN
            null;
    end validate_params;

    function get_linear_interpolation(
        i_variable               in number,
        i_x_values               in v_stage_blm_threshold_attrs.vector_value%type,
        i_y_values               in v_stage_blm_threshold_attrs.vector_value%type
    ) return number
        ------------------------------------------------------------------------------------------------------------------------
--  Description:
--    	   This function calculates a linear interpolation for the given variable
--
------------------------------------------------------------------------------------------------------------------------
--  Input Arguments:
--   i_variable - the variable
--   i_x_values - the collection of data points' x coordinates
--   i_y_values - the collection of data points' y coordinates
------------------------------------------------------------------------------------------------------------------------
--  Exceptions:
--    Raises commons error event BLM_THRESH_WRONG_COORD_SIZE when the size of X and Y coordinates does not match
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--    2014-01-28 : Marcin Sobieszek
--    Creation
------------------------------------------------------------------------------------------------------------------------
        is
        l_x0                        number := 0;
        l_y0                        number := 0;
        l_x1                        number;
        l_y1                        number;
    begin
        if i_x_values.count != i_y_values.count then
            com_event_mgr.raise_error_event('BLM_THRESH_WRONG_COORD_SIZE', 'The size of X and Y coordinates does not match' ||dbms_utility.format_error_stack);
        end if;

        if i_variable < i_x_values(1) then
            l_x1 := i_x_values(1);
            l_y1 := i_y_values(1);
        else
            for i in 1 .. i_x_values.count loop
                if i_x_values(i) = i_variable then
                    return i_y_values(i);
                end if;
                if i_variable < i_x_values(i)
                    or i = i_x_values.count then
                    l_x0 := i_x_values(i - 1);
                    l_y0 := i_y_values(i - 1);
                    l_x1 := i_x_values(i);
                    l_y1 := i_y_values(i);
                    exit;
                end if;
            end loop;
        end if;

        return com_math.get_linear_interpolation_for(l_x0, l_y0, l_x1, l_y1, i_variable);
    end get_linear_interpolation;

    function get_required_corrections(
        i_params                 in lsa_blm_types.t_params
    ) return lsa_blm_types.t_params
        ------------------------------------------------------------------------------------------------------------------------
--  Description:
--    	   This function gets the list of records of required corrections defined within the given configuration set.
--         Note: the returned collection is sorted by correction parameter index (ascending order) as the order in which
--         corrections are applied is important; we asume that the input collection is already properly sorted.
--
------------------------------------------------------------------------------------------------------------------------
--  Input Arguments:
--   i_params - the supported configuration parameters
------------------------------------------------------------------
--  Returns
--    the list of corresponding parameter records; an empty list if there is no corrections required
------------------------------------------------------------------------------------------------------------------------
--  Exceptions:
--    No known exceptions
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--    2014-01-20 : Marcin Sobieszek
--    Creation
------------------------------------------------------------------------------------------------------------------------
        is
        l_params                    lsa_blm_types.t_params;
        l_order                     integer := -1;
    begin
        for i in 1 .. i_params.count loop
            if l_order = i_params(i).operation_order then
                continue;
            end if;
            for r_corr in 1 .. lsa_blm_types.c_corrections.count loop
                if i_params(i).operation_name = lsa_blm_types.c_corrections(r_corr) then
                    l_params(l_params.count + 1) := i_params(i);
                    l_order := i_params(i).operation_order;
                    exit;
                end if;
            end loop;
        end loop;

        return l_params;
    end get_required_corrections;

    procedure apply_adhoc_corrections(
        i_corr_name              in blm_threshold_parameters.parameter_name%type,
        io_thresholds            in out lsa_blm_types.t_thresholds,
        i_params                 in lsa_blm_types.t_params,
        i_correction_order       in v_stage_blm_threshold_attrs.action_order%type
    )
        ------------------------------------------------------------------------------------------------------------------------
--  Description:
--    	   This procedure applies ad hoc corrections on the given collection of thresholds
--
------------------------------------------------------------------------------------------------------------------------
--  Input Arguments:
--   i_corr_name   - the name of the adhoc correction
--   io_thresholds - the collection of thresholds
--   i_params      - the supported configuration parameters
------------------------------------------------------------------
--  Returns
--    the modified collection of thresholds
------------------------------------------------------------------------------------------------------------------------
--  Exceptions:
--    Raises commons error event BLM_THRESH_UNKNOWN_CORR_TYPE when the correction type is not known.
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--    2014-01-20 : Marcin Sobieszek
--    Creation
------------------------------------------------------------------------------------------------------------------------
        is
        l_beam_levels               v_stage_blm_threshold_attrs.vector_value%type;
        l_rsum                      v_stage_blm_threshold_attrs.vector_value%type;
    begin
        case i_corr_name
            when lsa_blm_types.c_adhoc_corr_factor then
                l_beam_levels := get_param_values_for(i_params, lsa_blm_types.c_adhoc_corr_factor_elev, i_correction_order);
                l_rsum        := get_param_values_for(i_params, lsa_blm_types.c_adhoc_corr_factor_rsum, i_correction_order);
                lsa_blm_threshold_corrections.apply_adhoc_factor_correction(io_thresholds, l_beam_levels, l_rsum);
            when lsa_blm_types.c_adhoc_corr_fix2rs then
                l_beam_levels := get_param_values_for(i_params, lsa_blm_types.c_adhoc_corr_fix2rs_elev, i_correction_order);
                l_rsum        := get_param_values_for(i_params, lsa_blm_types.c_adhoc_corr_fix2rs_rsum, i_correction_order);
                lsa_blm_threshold_corrections.apply_adhoc_fix_rs_correction(io_thresholds, l_beam_levels, l_rsum);
            when lsa_blm_types.c_adhoc_corr_bits then
                l_beam_levels := get_param_values_for(i_params, lsa_blm_types.c_adhoc_corr_bits_elev, i_correction_order);
                l_rsum        := get_param_values_for(i_params, lsa_blm_types.c_adhoc_corr_bits_fix_bits, i_correction_order);
                lsa_blm_threshold_corrections.apply_adhoc_bits_correction(io_thresholds, l_beam_levels, l_rsum,
                                                                          get_param_value_for(i_params, lsa_blm_types.c_blm_conv_bit_2gy));
            else
                com_event_mgr.raise_error_event('BLM_THRESH_UNKNOWN_CORR_TYPE', 'Unknown correction type = ' || i_corr_name ||dbms_utility.format_error_stack);
            end case;

    end apply_adhoc_corrections;

    function is_adhoc_correction(
        i_corr_name              in blm_threshold_parameters.parameter_name%type
    ) return boolean
        ------------------------------------------------------------------------------------------------------------------------
--  Description:
--    	   This function checks if the given correction name belongs to the set of so called adhoc corrections.
--
------------------------------------------------------------------------------------------------------------------------
--  Input Arguments:
--   i_corr_name - the name of the correction
------------------------------------------------------------------
--  Returns
--    true if the given correction name is known as so called adhoc correction; false otherwise
------------------------------------------------------------------------------------------------------------------------
--  Exceptions:
--   No known exceptions
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--    2014-01-20 : Marcin Sobieszek
--    Creation
------------------------------------------------------------------------------------------------------------------------
        is
    begin
        for i in 1 .. lsa_blm_types.c_adhoc_corrections.count loop
            if lsa_blm_types.c_adhoc_corrections(i) = i_corr_name then
                return true;
            end if;
        end loop;
        return false;
    end is_adhoc_correction;

    procedure printInfo(
        l_thresholds                in lsa_blm_types.t_thresholds
    )
        is
        l_output                       varchar2(4000);
    begin
        for i in 1 .. l_thresholds.count loop
            l_output := '';
            for j in 1 .. l_thresholds(i).count loop
                l_output := l_output || l_thresholds(i)(j) || ' ';
            end loop;
            com_logger.debug(i || ' ' || l_output, $$PLSQL_UNIT);
        end loop;
    end;

    function apply_corrections_if_needed(
        i_thresholds             in lsa_blm_types.t_thresholds,
        i_params                 in lsa_blm_types.t_params
    ) return lsa_blm_types.t_thresholds
        ------------------------------------------------------------------------------------------------------------------------
--  Description:
--    	   This function applies corrections to the calculated thresholds in order to take into account effects
--         related to: modification of signals due to RC filters, specific injection losses etc. The calculated values
--         are rounded down.
--
------------------------------------------------------------------------------------------------------------------------
--  Input Arguments:
--   l_thresholds - the calculated thresholds
--   i_params     - the required configuration parameters
------------------------------------------------------------------------------------------------------------------------
--  Returns
--    the modified thresholds
------------------------------------------------------------------------------------------------------------------------
--  Exceptions:
--    No known exceptions
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--    2014-01-20 : Marcin Sobieszek - Creation
--    2014-06-01 : Marcin Sobieszek - BLMDM-113 - Modification of MinBitsCorrection
------------------------------------------------------------------------------------------------------------------------
        is
        l_thresholds                lsa_blm_types.t_thresholds;
        l_corrections               lsa_blm_types.t_params;
        l_corr_decrease_rs          boolean := true;
        l_corr_decrease_el          boolean := true;
        l_corr_max_bits_factor      number := 1;
    begin
        l_thresholds  := i_thresholds;
        l_corrections := get_required_corrections(i_params);
        for r_i in 1 .. l_corrections.count loop
            com_logger.debug('Applying correction: ' || l_corrections(r_i).operation_name, $$PLSQL_UNIT);
            case l_corrections(r_i).operation_name
                when lsa_blm_types.c_rc_correction then
                    lsa_blm_threshold_corrections.apply_rc_correction(l_thresholds, get_param_value_for(i_params, lsa_blm_types.c_rc_correction_tc, false, l_corrections(r_i).operation_order));
                when lsa_blm_types.c_il_correction then
                    lsa_blm_threshold_corrections.apply_il_correction(l_thresholds, get_param_value_for(i_params, lsa_blm_types.c_il_correction_tc, false, l_corrections(r_i).operation_order),
                                                                      get_param_value_for(i_params, lsa_blm_types.c_blm_conv_bit_2gy), get_param_value_for(i_params, lsa_blm_types.c_il_correction_norm, false, l_corrections(r_i).operation_order));
                when lsa_blm_types.c_scale_correction then
                    lsa_blm_threshold_corrections.apply_scale_correction(l_thresholds, get_param_value_for(i_params, lsa_blm_types.c_corr_scale_factor, false, l_corrections(r_i).operation_order));
                when lsa_blm_types.c_correct_decrease then
                    if coalesce(get_param_value_for(i_params, lsa_blm_types.c_correct_decrease_rs, false, l_corrections(r_i).operation_order), 1) = 0 then
                        l_corr_decrease_rs := false;
                    end if;
                    if coalesce(get_param_value_for(i_params, lsa_blm_types.c_correct_decrease_el, false, l_corrections(r_i).operation_order), 1) = 0 then
                        l_corr_decrease_el := false;
                    end if;
                    lsa_blm_threshold_corrections.apply_decrease_correction(l_thresholds, l_corr_decrease_rs, l_corr_decrease_el);
                when lsa_blm_types.c_correct_max_bits then
                    l_corr_max_bits_factor := coalesce(get_param_value_for(i_params, lsa_blm_types.c_correct_max_bits_fr, false, l_corrections(r_i).operation_order), 1);
                    lsa_blm_threshold_corrections.apply_max_bits_correction(l_thresholds, l_corr_max_bits_factor, vectornumeric());
                when lsa_blm_types.c_correct_min_bits then
                    lsa_blm_threshold_corrections.apply_min_bits_correction(l_thresholds, get_param_value_for(i_params, lsa_blm_types.c_correct_min_bits_f_param, false, l_corrections(r_i).operation_order));
                when lsa_blm_types.c_correct_off_bits then
                    lsa_blm_threshold_corrections.apply_off_bits_correction(l_thresholds, vectornumeric());
                else
                    null;
                end case;
            if is_adhoc_correction(l_corrections(r_i).operation_name) then
                apply_adhoc_corrections(l_corrections(r_i).operation_name, l_thresholds, i_params, l_corrections(r_i).operation_order);
            end if;
            printInfo(l_thresholds);
        end loop;

        -- round down values
        for r_energy in 1 .. l_thresholds.count loop
            for r_int_time in 1 .. l_thresholds(r_energy).count loop
                        l_thresholds(r_energy)(r_int_time) := floor(l_thresholds(r_energy)(r_int_time));
            end loop;
        end loop;

        return l_thresholds;
    end apply_corrections_if_needed;

    function prepare_expr(
        i_function               in varchar2,
        i_function_params        in v_stage_blm_threshold_attrs.vector_value%type
    ) return varchar2
        ------------------------------------------------------------------------------------------------------------------------
--  Description:
--    	   This function prepares math expression provided by the given function pattern.
--
------------------------------------------------------------------------------------------------------------------------
--  Input Arguments:
--   i_function        - the function pattern
--   i_function_params - the collection of variables to be bound
------------------------------------------------------------------------------------------------------------------------
--  Returns
--    the prepared math expression
------------------------------------------------------------------------------------------------------------------------
--  Exceptions:
--    No known exceptions
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--    2015-01-21 : Marcin Sobieszek
--    Creation
------------------------------------------------------------------------------------------------------------------------
        is
        l_function                  varchar2(300);
        l_param_index               varchar2(20);
        l_params_count              integer := 10;
    begin
        l_function := i_function;
        for i in 1 .. l_params_count loop
            l_param_index := '[' || (i - 1) ||']';
            if instr(l_function, l_param_index) > 0 then
                l_function := replace(l_function, l_param_index, i_function_params(i));
            end if;
        end loop;
        l_function := replace(l_function, 'log', 'ln');
        return l_function;
    end;

    function calculate_expr(
        i_function               in varchar2,
        i_function_params        in v_stage_blm_threshold_attrs.vector_value%type,
        i_variable               in number
    ) return number
        ------------------------------------------------------------------------------------------------------------------------
--  Description:
--    	   This function calculates math expression provided by the given function pattern. The expected pattern
--         can be parametrized; currently two parameters are supported with the following placeholders: [0] and [1].
--
------------------------------------------------------------------------------------------------------------------------
--  Input Arguments:
--   i_function        - the function pattern
--   i_function_params - the collection of variables to be bound
--   i_variable        - the variable of the function
------------------------------------------------------------------------------------------------------------------------
--  Returns
--    the calculated result
------------------------------------------------------------------------------------------------------------------------
--  Exceptions:
--    No known exceptions
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--    2014-01-27 : Marcin Sobieszek
--    Creation
------------------------------------------------------------------------------------------------------------------------
        is
        l_function                  varchar2(200);
        l_value                     number;
        l_occurences                number;
    begin
        l_function := prepare_expr(i_function, i_function_params);
        l_function := 'select to_number(' || l_function ||') from (select :x x from dual)';
        execute immediate l_function into l_value using i_variable;

        return l_value;
    end calculate_expr;

    function get_resp_energy_bits(
        i_params                 in lsa_blm_types.t_params,
        i_energy                 in number,
        i_time                   in number
    ) return number
        ------------------------------------------------------------------------------------------------------------------------
--  Description:
--    	   This function returns the value of the response [BITS] per lost proton.
------------------------------------------------------------------------------------------------------------------------
--  Input Arguments:
--   i_params - the required configuration parameters
--   i_energy - beam energy
------------------------------------------------------------------------------------------------------------------------
--  Returns
--    the calculated value
------------------------------------------------------------------------------------------------------------------------
--  Exceptions:
--    No known exceptions
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--    2014-01-20 : Marcin Sobieszek
--    Creation
------------------------------------------------------------------------------------------------------------------------
        is
        l_res_energy_bits           number;
        l_blm_conv_gy_2c            number;
        l_blm_conv_bit_2gy          number;
        l_blm_corr_rs               number;
        l_blm_corr_rs_values        v_stage_blm_threshold_attrs.vector_value%type;
    begin
        l_blm_conv_gy_2c     := get_param_value_for(i_params, lsa_blm_types.c_blm_conv_gy_2c);
        l_blm_conv_bit_2gy   := get_param_value_for(i_params, lsa_blm_types.c_blm_conv_bit_2gy);
        l_res_energy_bits    := calculate_expr(get_config_value_for(i_params, lsa_blm_types.c_blm_resp_form),
                                               get_param_values_for(i_params, lsa_blm_types.c_blm_resp_form_param), i_energy);
        l_blm_corr_rs_values := get_param_values_for(i_params, lsa_blm_types.c_blm_corr_rs);
        l_blm_corr_rs        := lsa_blm_types.c_correction_rs(i_time);
        if l_blm_corr_rs_values.count >= i_time then
            l_blm_corr_rs := l_blm_corr_rs_values(i_time);
        end if;
        return (1e-18 * l_res_energy_bits * l_blm_corr_rs * lsa_blm_types.c_blm_conva_c2) / (l_blm_conv_gy_2c * l_blm_conv_bit_2gy);
    end get_resp_energy_bits;

    function get_coll_correction_for(
        i_coll_corrections       in v_stage_blm_threshold_attrs.vector_value%type,
        i_index                  in number
    ) return number
        ------------------------------------------------------------------------------------------------------------------------
--  Description:
--    	   This function returns the appropriate factor of the fast correction for the given integration time represented
--         here as the index of the integration times table.
--
------------------------------------------------------------------------------------------------------------------------
--  Input Arguments:
--   i_coll_corrections - the collection of fast correction factors
--   i_index            - the integration time index
------------------------------------------------------------------------------------------------------------------------
--  Returns
--    the calculated value
------------------------------------------------------------------------------------------------------------------------
--  Exceptions:
--    No known exceptions
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--    2014-01-27 : Marcin Sobieszek
--    Creation
------------------------------------------------------------------------------------------------------------------------
        is
    begin
        if i_coll_corrections.count >= i_index then
            return i_coll_corrections(i_index);
        end if;
        return 1;
    end get_coll_correction_for;

    function get_rs_no_for_integration_time(
        i_time                   in number
    ) return number
        ------------------------------------------------------------------------------------------------------------------------
--  Description:
--    	   This function gets integration time index for a given integration time value. It makes sense only for
--         the first 5 indexes.
------------------------------------------------------------------------------------------------------------------------
--  Input Arguments:
--   i_time        - the integration time
------------------------------------------------------------------------------------------------------------------------
--  Returns
--    the integration time index
------------------------------------------------------------------------------------------------------------------------
--  Exceptions:
--    No known exceptions
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--    2014-02-07 : Marcin Sobieszek
--    Creation
------------------------------------------------------------------------------------------------------------------------

        is
    begin
        return
            case i_time
                when lsa_blm_types.c_integration_times(1) then
                    1
                when lsa_blm_types.c_integration_times(2) then
                    2
                when lsa_blm_types.c_integration_times(3) then
                    3
                when lsa_blm_types.c_integration_times(4) then
                    4
                when lsa_blm_types.c_integration_times(5) then
                    5
                else
                    6
                end;
    end get_rs_no_for_integration_time;

    function apply_fast_losses_correction(
        i_no_lost_protons        in number,
        i_time                   in number,
        i_params                 in lsa_blm_types.t_params
    ) return number
        ------------------------------------------------------------------------------------------------------------------------
--  Description:
--    	   This function applies fast losses corrections for the given number of lost protons.
--
------------------------------------------------------------------------------------------------------------------------
--  Input Arguments:
--   i_current_res - the current number of lost protons
--   i_time        - the integration time
--   i_params      - the required configuration parameters
------------------------------------------------------------------------------------------------------------------------
--  Returns
--    the modified value
------------------------------------------------------------------------------------------------------------------------
--  Exceptions:
--    No known exceptions
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--    2014-01-27 : Marcin Sobieszek
--    Creation
------------------------------------------------------------------------------------------------------------------------
        is
        l_value                     number := 1;
        l_rs_no                     number;
        l_coll_corr_rs              v_stage_blm_threshold_attrs.vector_value%type;
    begin
        l_coll_corr_rs := get_param_values_for(i_params, lsa_blm_types.c_coll_corr_rs);
        l_rs_no        := get_rs_no_for_integration_time(i_time);
        l_value        := case when l_rs_no < 5 then get_coll_correction_for(l_coll_corr_rs, l_rs_no) else 1 end;

        return i_no_lost_protons * l_value;
    end apply_fast_losses_correction;

    function get_coll_protons_fast_losses(
        i_energy                 in number,
        i_time                   in number,
        i_params                 in lsa_blm_types.t_params
    ) return number
        --  Description:
--    	   This function calculates the maximum number of lost protons for the given enery and loss duration for
--         Collimators with slow losses corrections
--
------------------------------------------------------------------------------------------------------------------------
--  Input Arguments:
--   i_energy - the energy
--   i_time   - loss duration
--   i_params - the required configuration parameters
------------------------------------------------------------------------------------------------------------------------
--  Returns
--    the number of lost protons
------------------------------------------------------------------------------------------------------------------------
--  Exceptions:
--    No known exceptions
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--    2014-01-24 : Marcin Sobieszek
--    Creation
------------------------------------------------------------------------------------------------------------------------
        is
        l_coll_np_short_inj      number;
        l_coll_np_short_coll     number;
        l_slope                  number;
        l_offset                 number;
        l_res                    number;
    begin
        l_coll_np_short_inj  := get_param_value_for(i_params, lsa_blm_types.c_coll_np_short_inj);
        l_coll_np_short_coll := get_param_value_for(i_params, lsa_blm_types.c_coll_np_short_coll);
        l_slope              := (l_coll_np_short_coll - l_coll_np_short_inj) / lsa_blm_types.c_energy_diff;
        l_offset             := l_coll_np_short_inj - (l_slope * lsa_blm_types.c_eInjection_tev);
        l_res                := l_offset + l_slope * i_energy;

        return apply_fast_losses_correction(l_res, i_time, i_params);
    end get_coll_protons_fast_losses;

    function get_coll_protons_slow_losses(
        i_energy                 in number,
        i_params                 in lsa_blm_types.t_params
    ) return number
        --  Description:
--    	   This function calculates the maximum number of lost protons for the given enery and loss duration for
--         Collimators with slow losses corrections
--
------------------------------------------------------------------------------------------------------------------------
--  Input Arguments:
--   i_energy - the energy
--   i_time   - loss duration
--   i_params - the required configuration parameters
------------------------------------------------------------------------------------------------------------------------
--  Returns
--    the number of lost protons
------------------------------------------------------------------------------------------------------------------------
--  Exceptions:
--    No known exceptions
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--    2014-01-24 : Marcin Sobieszek
--    Creation
------------------------------------------------------------------------------------------------------------------------
        is
        l_coll_dndt_mid_coll      number;
        l_coll_dndt_mid_inj       number;
        l_slope                   number;
        l_offset                  number;
    begin
        l_coll_dndt_mid_inj  := get_param_value_for(i_params, lsa_blm_types.c_coll_dndt_mid_inj);
        l_coll_dndt_mid_coll := get_param_value_for(i_params, lsa_blm_types.c_coll_dndt_mid_coll);
        l_slope              := (l_coll_dndt_mid_coll - l_coll_dndt_mid_inj) / lsa_blm_types.c_energy_diff;
        l_offset             := l_coll_dndt_mid_inj - (l_slope * lsa_blm_types.c_eInjection_tev);

        return (l_offset + l_slope * i_energy);
    end get_coll_protons_slow_losses;

    function get_coll_protons_steady_losses(
        i_energy                 in number,
        i_time                   in number,
        i_params                 in lsa_blm_types.t_params
    ) return number
        ------------------------------------------------------------------------------------------------------------------------
--  Description:
--    	   This function calculates the maximum number of lost protons for the given energy and loss duration for
--         Collimators with steady losses corrections
--
------------------------------------------------------------------------------------------------------------------------
--  Input Arguments:
--   i_energy - the energy
--   i_time   - loss duration
--   i_params - the required configuration parameters
------------------------------------------------------------------------------------------------------------------------
--  Returns
--    the number of lost protons
------------------------------------------------------------------------------------------------------------------------
--  Exceptions:
--    No known exceptions
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--    2014-01-24 : Marcin Sobieszek
--    Creation
------------------------------------------------------------------------------------------------------------------------
        is
        l_coll_dndt_long_coll    number;
        l_coll_dndt_long_inj     number;
        l_slope                  number;
        l_offset                 number;
        l_steady_losses          number;
    begin
        l_coll_dndt_long_inj  := get_param_value_for(i_params, lsa_blm_types.c_coll_dndt_long_inj);
        l_coll_dndt_long_coll := get_param_value_for(i_params, lsa_blm_types.c_coll_dndt_long_coll);
        l_slope               := (l_coll_dndt_long_coll - l_coll_dndt_long_inj) / lsa_blm_types.c_energy_diff;
        l_offset              := l_coll_dndt_long_inj - (l_slope * lsa_blm_types.c_eInjection_tev);
        l_steady_losses       := get_coll_protons_slow_losses(i_energy, i_params) * lsa_blm_types.c_slow_lost_time;

        return l_steady_losses + (i_time - lsa_blm_types.c_slow_lost_time) * (l_offset + l_slope * i_energy);
    end get_coll_protons_steady_losses;

    function get_coll_max_lost_protons(
        i_energy                 in number,
        i_time                   in number,
        i_params                 in lsa_blm_types.t_params
    ) return number
        ------------------------------------------------------------------------------------------------------------------------
--  Description:
--    	   This function calculates the maximum number of lost protons for the given enery and loss duration for
--         Collimators
--
------------------------------------------------------------------------------------------------------------------------
--  Input Arguments:
--   i_energy - the energy
--   i_time   - loss duration
--   i_params - the required configuration parameters
------------------------------------------------------------------------------------------------------------------------
--  Returns
--    the number of lost protons
------------------------------------------------------------------------------------------------------------------------
--  Exceptions:
--    No known exceptions
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--    2014-01-20 : Marcin Sobieszek
--    Creation
------------------------------------------------------------------------------------------------------------------------
        is
    begin
        dbms_output.put_line('executing get_coll_max_lost_protons');

        return
            case
                when i_time < lsa_blm_types.c_fast_lost_time then get_coll_protons_fast_losses(i_energy, i_time, i_params)
                when i_time < lsa_blm_types.c_slow_lost_time then  get_coll_protons_slow_losses(i_energy, i_params) * i_time
                else get_coll_protons_steady_losses(i_energy, i_time, i_params)
                end;
    end get_coll_max_lost_protons;

    function get_coll_run3_protons_fast_losses(
        i_energy                 in number,
        i_time                   in number,
        i_params                 in lsa_blm_types.t_params
    ) return number
        ------------------------------------------------------------------------------------------------------------------------
--  Description:
--    	   This function calculates the maximum number of lost protons for the given energy and loss duration for
--         Collimators with slow losses corrections
--
------------------------------------------------------------------------------------------------------------------------
--  Input Arguments:
--   i_energy - the energy
--   i_time   - loss duration
--   i_params - the required configuration parameters
------------------------------------------------------------------------------------------------------------------------
--  Returns
--    the number of lost protons
------------------------------------------------------------------------------------------------------------------------
--  Exceptions:
--    No known exceptions
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--    2021-09-20 : Maciej Peryt
--    Creation
------------------------------------------------------------------------------------------------------------------------
        is
        l_coll_np_short_inj      number;
        l_res                    number;
    begin
        l_coll_np_short_inj  := get_param_value_for(i_params, lsa_blm_types.c_coll_np_short_inj);
        l_res                :=  l_coll_np_short_inj * lsa_blm_types.c_eInjection_tev / i_energy;

        return apply_fast_losses_correction(l_res, i_time, i_params);
    end get_coll_run3_protons_fast_losses;

    function get_coll_run3_protons_slow_losses(
        i_energy                 in number,
        i_time                   in number,
        i_params                 in lsa_blm_types.t_params
    ) return number
        ------------------------------------------------------------------------------------------------------------------------
--  Description:
--    	   This function calculates the maximum number of lost protons for the given enery and loss duration for
--         Collimators with slow losses corrections
--
------------------------------------------------------------------------------------------------------------------------
--  Input Arguments:
--   i_energy - the energy
--   i_time   - loss duration
--   i_params - the required configuration parameters
------------------------------------------------------------------------------------------------------------------------
--  Returns
--    the number of lost protons
------------------------------------------------------------------------------------------------------------------------
--  Exceptions:
--    No known exceptions
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--    2021-09-20 : Maciej Peryt
--    Creation
------------------------------------------------------------------------------------------------------------------------
        is
        l_coll_dndt_mid_inj       number;
        l_res                    number;
    begin
        l_coll_dndt_mid_inj  := get_param_value_for(i_params, lsa_blm_types.c_coll_dndt_mid_inj);

        l_res                :=  l_coll_dndt_mid_inj * lsa_blm_types.c_eInjection_tev / i_energy;

        if i_time < 0.5 then
            return l_res * 0.65536;
        elsif i_time > 10.0 then
            return l_res * 10.0;
        end if;

        return l_res * i_time;
    end get_coll_run3_protons_slow_losses;

    function get_coll_run3_protons_steady_losses(
        i_energy                 in number,
        i_time                   in number,
        i_params                 in lsa_blm_types.t_params
    ) return number
        ------------------------------------------------------------------------------------------------------------------------
--  Description:
--    	   This function calculates the maximum number of lost protons for the given energy and loss duration for
--         Collimators with steady losses corrections
--
------------------------------------------------------------------------------------------------------------------------
--  Input Arguments:
--   i_energy - the energy
--   i_time   - loss duration
--   i_params - the required configuration parameters
------------------------------------------------------------------------------------------------------------------------
--  Returns
--    the number of lost protons
------------------------------------------------------------------------------------------------------------------------
--  Exceptions:
--    No known exceptions
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--    2021-09-20 : Maciej Peryt
--    Creation
------------------------------------------------------------------------------------------------------------------------
        is
        l_coll_dndt_long_inj     number;

    begin
        l_coll_dndt_long_inj  := get_param_value_for(i_params, lsa_blm_types.c_coll_dndt_long_inj);
        return l_coll_dndt_long_inj * lsa_blm_types.c_eInjection_tev / i_energy * i_time;
    end get_coll_run3_protons_steady_losses;

    function get_coll_run3_max_lost_protons(
        i_energy                 in number,
        i_time                   in number,
        i_params                 in lsa_blm_types.t_params
    ) return number
        ------------------------------------------------------------------------------------------------------------------------
--  Description:
--    	   This function calculates the maximum number of lost protons for the given enery and loss duration for
--         Collimators
--
------------------------------------------------------------------------------------------------------------------------
--  Input Arguments:
--   i_energy - the energy
--   i_time   - loss duration
--   i_params - the required configuration parameters
------------------------------------------------------------------------------------------------------------------------
--  Returns
--    the number of lost protons
------------------------------------------------------------------------------------------------------------------------
--  Exceptions:
--    No known exceptions
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--    2021-09-20 : Maciej Peryt
--    Creation
------------------------------------------------------------------------------------------------------------------------
        is
    begin
        dbms_output.put_line('executing get_coll_run3_max_lost_protons');

        return
            case
                when i_time < lsa_blm_types.c_r3_fast_lost_time then get_coll_run3_protons_fast_losses(i_energy, i_time, i_params)
                when i_time < lsa_blm_types.c_r3_slow_lost_time then  get_coll_run3_protons_slow_losses(i_energy, i_time, i_params)
                else get_coll_run3_protons_steady_losses(i_energy, i_time, i_params)
                end;
    end get_coll_run3_max_lost_protons;

    function get_wm_max_lost_protons(
        i_energy                 in number,
        i_time                   in number,
        i_params                 lsa_blm_types.t_params
    ) return number
        ------------------------------------------------------------------------------------------------------------------------
--  Description:
--    	   This function calculates the maximum number of lost protons for the given enery and loss duration for
--         Warm Magnets
--
------------------------------------------------------------------------------------------------------------------------
--  Input Arguments:
--   i_energy - the energy
--   i_time   - loss duration
--   i_params - the required configuration parameters
------------------------------------------------------------------------------------------------------------------------
--  Returns
--    the number of lost protons
------------------------------------------------------------------------------------------------------------------------
--  Exceptions:
--    No known exceptions
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--    2014-01-20 : Marcin Sobieszek - Creation
--    2015-06-01 : Marcin Sobieszek - BLMDM-110, Modify the way the number of lost protons is calculated
------------------------------------------------------------------------------------------------------------------------
        is
        l_n_short                   number;
        l_n_long                    number;
        l_dtime                     number;
        l_time_weight               number;
    begin
        l_dtime         := ln(lsa_blm_types.c_integration_times(12)) - ln(lsa_blm_types.c_integration_times(1));
        l_time_weight   := (ln(i_time) - ln(lsa_blm_types.c_integration_times(1))) / l_dtime;
        l_n_long        := calculate_expr(get_config_value_for(i_params, lsa_blm_types.c_wm_np_long_form),
                                          get_param_values_for(i_params, lsa_blm_types.c_wm_np_long_param), i_energy) * i_time;
        l_n_short       := calculate_expr(get_config_value_for(i_params, lsa_blm_types.c_wm_np_short_form),
                                          get_param_values_for(i_params, lsa_blm_types.c_wm_np_short_param), i_energy);

        return ((1 - l_time_weight) * l_n_short) + (l_time_weight * l_n_long);
    end get_wm_max_lost_protons;

    function get_rs_index_for(
        i_time                   in number
    ) return number
        ------------------------------------------------------------------------------------------------------------------------
--  Description:
--    	   This function returns the index of given rs sum value from lsa_blm_types.c_integration_times array.
--
------------------------------------------------------------------------------------------------------------------------
--  Input Arguments:
--   i_time   - the running sum value
------------------------------------------------------------------------------------------------------------------------
--  Returns
--    the index for given rs value
------------------------------------------------------------------------------------------------------------------------
--  Exceptions:
--    BLM_WRONG_INPUT_DATA when i_time parameter is not found in predefined constant lsa_blm_types.c_integration_times
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--    2015-01-21 : Marcin Sobieszek
--    Creation
------------------------------------------------------------------------------------------------------------------------
        is
    begin
        for i in 1 .. lsa_blm_types.c_integration_times.count loop
            if coalesce(i_time, -1) = lsa_blm_types.c_integration_times(i) then
                return i;
            end if;
        end loop;
        com_event_mgr.raise_event('BLM_WRONG_INPUT_DATA', 'The i_time [' || i_time || '] value does not exist in predefined constants', true, true);
    end;

    function get_energy_index_for(
        i_energy                 in number
    ) return number
        ------------------------------------------------------------------------------------------------------------------------
--  Description:
--    	   This function returns the index of given energy value from lsa_blm_types.c_beam_energies array.
--
------------------------------------------------------------------------------------------------------------------------
--  Input Arguments:
--   i_energy   - the energy value
------------------------------------------------------------------------------------------------------------------------
--  Returns
--    the index for given energy value
------------------------------------------------------------------------------------------------------------------------
--  Exceptions:
--    BLM_WRONG_INPUT_DATA when i_time parameter is not found in predefined constant lsa_blm_types.c_beam_energies
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--    2015-01-21 : Marcin Sobieszek
--    Creation
------------------------------------------------------------------------------------------------------------------------
        is
    begin
        for i in 1 .. lsa_blm_types.c_beam_energies.count loop
            if coalesce(i_energy, -1) = lsa_blm_types.c_beam_energies(i) then
                return i;
            end if;
        end loop;
        com_event_mgr.raise_event('BLM_WRONG_INPUT_DATA', 'The i_energy [' || i_energy || '] value does not exist in predefined constants', true, true);
    end;

    function get_reference_quench_margin(
        i_energy                 in number,
        i_time                   in number,
        i_params                 in lsa_blm_types.t_params
    ) return number
        ------------------------------------------------------------------------------------------------------------------------
--  Description:
--    	   This function is supposed to return the quench level from predefined lookup table. In the near future it
--         is foreseen to estimate the quench levels with some simulation programs. In that case the quench levels
--         will be given a priori as a 12x32 lookup table. It returns valid values only when protected_elem_type is
--         equals to COLD_QP3
--
------------------------------------------------------------------------------------------------------------------------
--  Input Arguments:
--   i_energy - the energy
--   i_time   - the running sum
------------------------------------------------------------------------------------------------------------------------
--  Returns
--    the quench level
------------------------------------------------------------------------------------------------------------------------
--  Exceptions:
--    Raises commons error event BLM_NO_QP3_TABLE_DEFINED when there's no qp3 table defined for the family.
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--    2014-01-27 : Marcin Sobieszek
--    Creation
------------------------------------------------------------------------------------------------------------------------
        is
        l_qm                     number;
        l_stmt                   varchar2(200);
        l_time_index             integer;
        l_e_index                integer;
        l_qp3_table              lsa_blm_types.t_param;
    begin
        l_qp3_table := get_config_value_for(i_params, lsa_blm_types.c_qp3_table_param);
        if l_qp3_table is null then
            com_event_mgr.raise_event('BLM_NO_QP3_TABLE_DEFINED', 'No QP3 table defined'||dbms_utility.format_error_stack, true, true);
        end if;

        l_time_index := get_rs_index_for(i_time);
        l_e_index    := get_energy_index_for(i_energy);
        l_stmt := 'select time_rs' || lpad(l_time_index, 2, '0') || '_threshold ' ||
                  '  from blm_threshold_quench_qp3' ||
                  ' where beam_energy_pos = :1' ||
                  '   and qp3_table_name = :2';
        execute immediate l_stmt into l_qm using l_e_index, l_qp3_table;
        return l_qm;
    exception
        when NO_DATA_FOUND then
            return null;
    end get_reference_quench_margin;

    function get_transient_losses_qm(
        i_energy                 in number,
        i_params                 in lsa_blm_types.t_params
    ) return number
        ------------------------------------------------------------------------------------------------------------------------
--  Description:
--    	   This function gets quench margin transient losses, that is for the times shorter than the metallic
--         characteristic time.
--
------------------------------------------------------------------------------------------------------------------------
--  Input Arguments:
--   i_energy - the energy
--   i_params - the required configuration parameters
------------------------------------------------------------------------------------------------------------------------
--  Returns
--    the quench level
------------------------------------------------------------------------------------------------------------------------
--  Exceptions:
--    No known exceptions
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--    2014-01-27 : Marcin Sobieszek
--    Creation
------------------------------------------------------------------------------------------------------------------------
        is
        l_function                  lsa_blm_types.t_param;
    begin
        l_function := get_param_value_for(i_params, lsa_blm_types.c_mg_en_li_form);

        return
            case l_function
                when lsa_blm_types.c_interpolated then
                    get_linear_interpolation(i_energy, get_param_values_for(i_params, lsa_blm_types.c_mg_en_li_form_points_x),
                                             get_param_values_for(i_params, lsa_blm_types.c_mg_en_li_form_points_y))
                else
                    calculate_expr(get_config_value_for(i_params, lsa_blm_types.c_mg_en_li_form),
                                   get_param_values_for(i_params, lsa_blm_types.c_mg_en_li_form_param), i_energy)
                end;
    end get_transient_losses_qm;

    function get_quench_temperature(
        i_energy                 in number,
        i_params                 in lsa_blm_types.t_params
    ) return number
        ------------------------------------------------------------------------------------------------------------------------
--  Description:
--    	   This function calculates the quench temperature for the given beam energy.
--
------------------------------------------------------------------------------------------------------------------------
--  Input Arguments:
--   i_energy - the energy
--   i_params - the required configuration parameters
------------------------------------------------------------------------------------------------------------------------
--  Returns
--    the quench temperature
------------------------------------------------------------------------------------------------------------------------
--  Exceptions:
--    No known exceptions
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--    2014-01-27 : Marcin Sobieszek
--    Creation
------------------------------------------------------------------------------------------------------------------------
        is
        l_tq_slope                  number;
        l_tq_offset                 number;
    begin
        l_tq_slope  := coalesce(get_param_value_for(i_params, lsa_blm_types.c_t_quench_slope_param), lsa_blm_types.c_t_quench_slope);
        l_tq_offset := coalesce(get_param_value_for(i_params, lsa_blm_types.c_t_quench_offset_param), lsa_blm_types.c_t_quench_offset);

        return l_tq_slope * i_energy + l_tq_offset;
    end get_quench_temperature;

    function get_helium_heat_reserve(
        i_energy                 in number,
        i_params                 in lsa_blm_types.t_params
    ) return number
        ------------------------------------------------------------------------------------------------------------------------
--  Description:
--    	   This function calculates heat reserve in the cable.
--
------------------------------------------------------------------------------------------------------------------------
--  Input Arguments:
--   i_energy - the energy
--   i_params - the required configuration parameters
------------------------------------------------------------------------------------------------------------------------
--  Returns
--    the heat reserve in the cable
------------------------------------------------------------------------------------------------------------------------
--  Exceptions:
--    No known exceptions
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--    2014-01-27 : Marcin Sobieszek
--    Creation
------------------------------------------------------------------------------------------------------------------------
        is
        l_qt                        number;
    begin
        l_qt := get_quench_temperature(i_energy, i_params);

        return
            case
                when l_qt < lsa_blm_types.c_t_he_21 then
                            lsa_blm_types.c_q_he1_slope * l_qt + lsa_blm_types.c_q_he1_offset
                else
                        (lsa_blm_types.c_q_he2_slope * l_qt + lsa_blm_types.c_q_he2_offset) * 1000 -- converting to mJ
                end;
    end get_helium_heat_reserve;

    function get_intermediate_losses_qm(
        i_energy                 in number,
        i_time                   in number,
        i_params                 in lsa_blm_types.t_params,
        i_tau_helium             in number
    ) return number
        ------------------------------------------------------------------------------------------------------------------------
--  Description:
--    	   This function gets quench margin intermediate losses, that is for the times approaching the Helium
--         characteristic time
--
------------------------------------------------------------------------------------------------------------------------
--  Input Arguments:
--   i_energy - the energy
--   i_params - the required configuration parameters
------------------------------------------------------------------------------------------------------------------------
--  Returns
--    the quench level (number of protons)
------------------------------------------------------------------------------------------------------------------------
--  Exceptions:
--    No known exceptions
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--    2014-01-27 : Marcin Sobieszek
--    Creation
------------------------------------------------------------------------------------------------------------------------
        is
        l_dt_tau                    number;
        l_tau_transient             number;
    begin
        l_dt_tau        := i_time * lsa_blm_types.c_mg_he_frac / i_tau_helium;
        l_dt_tau        := least(l_dt_tau, lsa_blm_types.c_mg_he_frac);
        l_tau_transient := get_transient_losses_qm(i_energy, i_params);

        return l_tau_transient + l_dt_tau * get_helium_heat_reserve(i_energy, i_params);
    end get_intermediate_losses_qm;

    function get_ss_quench_margin(
        i_energy                 in number,
        i_params                 in lsa_blm_types.t_params
    ) return number
        ------------------------------------------------------------------------------------------------------------------------
--  Description:
--    	   This function gets steady state quench margin for the given beam energy
--
------------------------------------------------------------------------------------------------------------------------
--  Input Arguments:
--   i_energy - the energy
--   i_params - the required configuration parameters
------------------------------------------------------------------------------------------------------------------------
--  Returns
--    the quench level (number of protons)
------------------------------------------------------------------------------------------------------------------------
--  Exceptions:
--    No known exceptions
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--    2014-01-27 : Marcin Sobieszek
--    Creation
------------------------------------------------------------------------------------------------------------------------
        is
    begin
        return calculate_expr(get_config_value_for(i_params, lsa_blm_types.c_mg_qu_li_form),
                              get_param_values_for(i_params, lsa_blm_types.c_mg_qu_li_form_param), i_energy);
    end get_ss_quench_margin;

    function get_steady_losses_qm(
        i_energy                 in number,
        i_time                   in number,
        i_params                 in lsa_blm_types.t_params
    ) return number
        ------------------------------------------------------------------------------------------------------------------------
--  Description:
--    	   This function gets quench margin steady losses, that is for longer times
--
------------------------------------------------------------------------------------------------------------------------
--  Input Arguments:
--   i_energy - the energy
--   i_params - the required configuration parameters
------------------------------------------------------------------------------------------------------------------------
--  Returns
--    the quench level (number of protons)
------------------------------------------------------------------------------------------------------------------------
--  Exceptions:
--    No known exceptions
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--    2014-01-27 : Marcin Sobieszek
--    Creation
------------------------------------------------------------------------------------------------------------------------
        is
        l_dt_tau                    number;
    begin
        l_dt_tau := lsa_blm_types.c_mg_he_frac * get_helium_heat_reserve(i_energy, i_params);
        return l_dt_tau + get_ss_quench_margin(i_energy, i_params) * i_time;
    end get_steady_losses_qm;

    function calculate_quench_margin(
        i_energy                 in number,
        i_time                   in number,
        i_params                 in lsa_blm_types.t_params
    ) return number
        ------------------------------------------------------------------------------------------------------------------------
--  Description:
--    	   This function calculates the quench margin for a given beam energy and loss duration (time). It returns
--         the number of protons which will quench given magnet with 3 cases depending on the quench duration.
--
------------------------------------------------------------------------------------------------------------------------
--  Input Arguments:
--   i_energy - the energy
--   i_time   - loss duration (time)
--   i_params - the required configuration parameters
------------------------------------------------------------------------------------------------------------------------
--  Returns
--    the number of protons
------------------------------------------------------------------------------------------------------------------------
--  Exceptions:
--    No known exceptions
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--    2014-01-27 : Marcin Sobieszek
--    Creation
------------------------------------------------------------------------------------------------------------------------
        is
        l_tau_metal                 number;
        l_tau_helium                number;
    begin
        l_tau_metal  := calculate_expr(get_config_value_for(i_params, lsa_blm_types.c_mg_tau_me_form),
                                       get_param_values_for(i_params, lsa_blm_types.c_mg_tau_me_form_param), i_energy);
        l_tau_helium := calculate_expr(get_config_value_for(i_params, lsa_blm_types.c_mg_tau_he_form),
                                       get_param_values_for(i_params, lsa_blm_types.c_mg_tau_he_form_param), i_energy);

        return
            case
                when i_time < l_tau_metal then
                    get_transient_losses_qm(i_energy, i_params)
                when i_time < l_tau_helium then
                    get_intermediate_losses_qm(i_energy, i_time, i_params, l_tau_helium)
                else
                    get_steady_losses_qm(i_energy, i_time, i_params)
                end;
    end calculate_quench_margin;

    function get_tau_transient(
        i_energy                 in number,
        i_params                 in lsa_blm_types.t_params
    ) return number
        ------------------------------------------------------------------------------------------------------------------------
--  Description:
--    	   This function returns the characteristic time (in ms) for metal
--
------------------------------------------------------------------------------------------------------------------------
--  Input Arguments:
--   i_energy - the energy
--   i_params - the required configuration parameters
------------------------------------------------------------------------------------------------------------------------
--  Returns
--    the characteristic time for metal
------------------------------------------------------------------------------------------------------------------------
--  Exceptions:
--    No known exceptions
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--    2014-01-27 : Marcin Sobieszek
--    Creation
------------------------------------------------------------------------------------------------------------------------
        is
        l_tau_transient_user        number;
    begin
        l_tau_transient_user := get_param_value_for(i_params, lsa_blm_types.c_tau_transient_user);

        return coalesce(l_tau_transient_user, calculate_expr(get_config_value_for(i_params, lsa_blm_types.c_mg_tau_me_form),
                                                             get_param_values_for(i_params, lsa_blm_types.c_mg_tau_me_form_param), i_energy));
    end get_tau_transient;

    function get_tau_steady(
        i_energy                 in number,
        i_params                 in lsa_blm_types.t_params
    ) return number
        ------------------------------------------------------------------------------------------------------------------------
--  Description:
--    	   This function returns the characteristic time (in ms) for Helium
--
------------------------------------------------------------------------------------------------------------------------
--  Input Arguments:
--   i_energy - the energy
--   i_params - the required configuration parameters
------------------------------------------------------------------------------------------------------------------------
--  Returns
--    the characteristic time for Helium
------------------------------------------------------------------------------------------------------------------------
--  Exceptions:
--    No known exceptions
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--    2014-01-27 : Marcin Sobieszek
--    Creation
------------------------------------------------------------------------------------------------------------------------
        is
        l_tau_steady                number;
    begin
        l_tau_steady := get_param_value_for(i_params, lsa_blm_types.c_tau_steady_user);

        return coalesce(l_tau_steady, calculate_expr(get_config_value_for(i_params, lsa_blm_types.c_mg_tau_he_form),
                                                     get_param_values_for(i_params, lsa_blm_types.c_mg_tau_he_form_param), i_energy));
    end get_tau_steady;

    function get_heat_transfer_regime(
        i_energy                 in number,
        i_time                   in number,
        i_params                 in lsa_blm_types.t_params
    ) return lsa_blm_types.t_regime
        ------------------------------------------------------------------------------------------------------------------------
--  Description:
--    	   This function returns transfer regime for the given loss duration and beam energy
--
------------------------------------------------------------------------------------------------------------------------
--  Input Arguments:
--   i_energy - the energy
--   i_time   - loss duration (time)
--   i_params - the required configuration parameters
------------------------------------------------------------------------------------------------------------------------
--  Returns
--    the transfer regime, one of the following three: transient, intermediate, steady
------------------------------------------------------------------------------------------------------------------------
--  Exceptions:
--    No known exceptions
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--    2014-01-27 : Marcin Sobieszek
--    Creation
------------------------------------------------------------------------------------------------------------------------
        is
        l_tau_transient             number;
        l_tau_steady                number;
    begin
        l_tau_transient := get_tau_transient(i_energy, i_params);
        l_tau_steady    := get_tau_steady(i_energy, i_params);

        return
            case
                when i_time < l_tau_transient then
                    lsa_blm_types.c_regime_transient
                when i_time < l_tau_steady then
                    lsa_blm_types.c_regime_intermediate
                else
                    lsa_blm_types.c_regime_steady
                end;
    end get_heat_transfer_regime;

    function get_edep_max(
        i_energy                 in number,
        i_params                 in lsa_blm_types.t_params
    ) return number
        ------------------------------------------------------------------------------------------------------------------------
--  Description:
--    	   This function returns the maximum energy deposited during a loss for a given beam energy
--
------------------------------------------------------------------------------------------------------------------------
--  Input Arguments:
--   i_energy - the energy
--   i_params - the required configuration parameters
------------------------------------------------------------------------------------------------------------------------
--  Returns
--    the deposited energy
------------------------------------------------------------------------------------------------------------------------
--  Exceptions:
--    No known exceptions
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--    2014-01-27 : Marcin Sobieszek
--    Creation
------------------------------------------------------------------------------------------------------------------------
        is
        l_function                  lsa_blm_types.t_param;
    begin
        l_function := get_config_value_for(i_params, lsa_blm_types.c_loss_edep_max_form);
        return
            case l_function
                when lsa_blm_types.c_interpolated then
                    get_linear_interpolation(i_energy, get_param_values_for(i_params, lsa_blm_types.c_loss_edep_max_f_points_x),
                                             get_param_values_for(i_params, lsa_blm_types.c_loss_edep_max_f_points_y))
                else
                    calculate_expr(get_config_value_for(i_params, lsa_blm_types.c_loss_edep_max_form),
                                   get_param_values_for(i_params, lsa_blm_types.c_loss_edep_max_form_param), i_energy)
                end;
    end get_edep_max;

    function get_edep_thermal(
        i_energy                 in number,
        i_params                 in lsa_blm_types.t_params
    ) return number
        ------------------------------------------------------------------------------------------------------------------------
--  Description:
--    	   This function returns the energy deposited during a loss (averaged over thermal equlibrium volume)
--         for a given beam energy.
--
------------------------------------------------------------------------------------------------------------------------
--  Input Arguments:
--   i_energy - the energy
--   i_params - the required configuration parameters
------------------------------------------------------------------------------------------------------------------------
--  Returns
--    the deposited energy
------------------------------------------------------------------------------------------------------------------------
--  Exceptions:
--    No known exceptions
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--    2014-01-27 : Marcin Sobieszek
--    Creation
------------------------------------------------------------------------------------------------------------------------
        is
    begin
        return calculate_expr(get_config_value_for(i_params, lsa_blm_types.c_loss_edep_th_form),
                              get_param_values_for(i_params, lsa_blm_types.c_loss_edep_th_form_param), i_energy);
    end get_edep_thermal;

    function get_energy_deposit(
        i_energy                 in number,
        i_time                   in number,
        i_params                 in lsa_blm_types.t_params
    ) return number
        ------------------------------------------------------------------------------------------------------------------------
--  Description:
--    	   This function returns the energy deposited during a loss for a given beam energy.
--
------------------------------------------------------------------------------------------------------------------------
--  Input Arguments:
--   i_energy - the energy
--   i_time   - loss duration (time)
--   i_params - the required configuration parameters
------------------------------------------------------------------------------------------------------------------------
--  Returns
--    the deposited energy
------------------------------------------------------------------------------------------------------------------------
--  Exceptions:
--    No known exceptions
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--    2014-01-27 : Marcin Sobieszek
--    Creation
------------------------------------------------------------------------------------------------------------------------
        is
        l_regime                    lsa_blm_types.t_regime;
    begin
        l_regime := get_heat_transfer_regime(i_energy, i_time, i_params);

        return
            case  l_regime
                when lsa_blm_types.c_regime_transient then
                    get_edep_max(i_energy, i_params)
                when lsa_blm_types.c_regime_intermediate then
                    get_edep_thermal(i_energy, i_params)
                when lsa_blm_types.c_regime_steady then
                    get_edep_thermal(i_energy, i_params)
                end;
    end get_energy_deposit;

    function get_cm_nr_of_protons(
        i_energy                 in number,
        i_time                   in number,
        i_params                 in lsa_blm_types.t_params,
        i_protected_element_type in blm_protected_elem_types.element_type%type
    ) return number
        ------------------------------------------------------------------------------------------------------------------------
--  Description:
--    	   This function calculates the maximum number of lost protons for the given enery and loss duration for
--         Cold Magnets
--
------------------------------------------------------------------------------------------------------------------------
--  Input Arguments:
--   i_energy - the energy
--   i_time - loss duration
--   i_params - the required configuration parameters
------------------------------------------------------------------------------------------------------------------------
--  Returns
--    the number of lost protons
------------------------------------------------------------------------------------------------------------------------
--  Exceptions:
--    No known exceptions
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--    2014-01-27 : Marcin Sobieszek
--    Creation
------------------------------------------------------------------------------------------------------------------------
        is
        l_ret                       number := 1;
    begin
        if coalesce(i_protected_element_type, 'ala_ma_kota') = lsa_blm_types.c_cold_qp3 then
            return  get_reference_quench_margin(i_energy, i_time, i_params)
                        / get_edep_max(i_energy, i_params)
                * lsa_blm_types.c_safety_factor;
        end if;
        return calculate_quench_margin(i_energy, i_time, i_params)
                   / get_energy_deposit(i_energy, i_time, i_params)
            * lsa_blm_types.c_safety_factor;
    end get_cm_nr_of_protons;

    function get_protected_element_type (
        i_family_name            in blm_families.family_name%type
    )
        return lsa_blm_types.t_element_type_rec
        ------------------------------------------------------------------------------------------------------------------------
--  Description:
--    	   This function gets protected element type for the given BLM family name
------------------------------------------------------------------------------------------------------------------------
--  Input Arguments:
--   i_family_name - the BLM family name
------------------------------------------------------------------------------------------------------------------------
--  Returns
--    the protected element type
------------------------------------------------------------------------------------------------------------------------
--  Exceptions:
--    Raises commons error event NO_BLM_FAMILY when the given family does not already exist in the database
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--    2014-01-20 : Marcin Sobieszek
--    Creation
------------------------------------------------------------------------------------------------------------------------
        is
        l_ret                    lsa_blm_types.t_element_type_rec;
    begin
        select t2.parent_type, t2.element_type
        into l_ret.parent_type, l_ret.element_type
        from stage_blm_families t1
                 join blm_protected_elem_types t2 on (t1.protected_element_type = t2.element_type)
        where family_name = i_family_name;

        return l_ret;
    exception
        when no_data_found then
            com_event_mgr.raise_event('NO_BLM_FAMILY', 'The BLM family '||i_family_name||' does not exist'||dbms_utility.format_error_stack, true, true);
    end get_protected_element_type;

    function get_params(
        i_family_name            in blm_families.family_name%type
    )
        return lsa_blm_types.t_params
        ------------------------------------------------------------------------------------------------------------------------
--  Description:
--    	   This function gets configuration parameters for the given BLM family name
------------------------------------------------------------------------------------------------------------------------
--  Input Arguments:
--   i_family_name - the BLM family name
------------------------------------------------------------------------------------------------------------------------
--  Returns
--    the collection of configuration parameters
------------------------------------------------------------------------------------------------------------------------
--  Exceptions:
--    Raises commons error event NO_BLM_FAMILY when the given family does not have any parameters set
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--    2014-01-20 : Marcin Sobieszek
--    Creation
------------------------------------------------------------------------------------------------------------------------
        is
        l_params                    lsa_blm_types.t_params;
        l_index                     number;
    begin
        select operation_name, action_order, parameter_name, scalar_value, vector_value, config_value
            bulk collect into l_params
        from v_stage_blm_threshold_attrs
        where family_name = i_family_name
        order by action_order;

        l_index := l_params.count + 1;
        select lsa_blm_types.c_blm_conv_gy_2c, t2.conv_factor_gy_s_amps
        into l_params(l_index).param_name, l_params(l_index).param_value
        from stage_blm_families
                 join blm_family_conversion_factor t2 using (conversion_factor_name)
        where family_name = i_family_name;

        l_index := l_params.count + 1;
        select lsa_blm_types.c_blm_conv_bit_2gy, t2.conv_factor_blmbit_gy_s
        into l_params(l_index).param_name, l_params(l_index).param_value
        from stage_blm_families
                 join blm_family_conversion_factor t2 using (conversion_factor_name)
        where family_name = i_family_name
        ;

        return l_params;
    exception
        when no_data_found then
            com_event_mgr.raise_event('NO_BLM_FAMILY', 'The BLM family '||i_family_name||' does not have any parameters set '||dbms_utility.format_error_stack, true, true);
    end get_params;

    function get_max_lost_protons(
        i_element_type_rec       in lsa_blm_types.t_element_type_rec,
        i_energy                 in number,
        i_time                   in number,
        i_params                 in lsa_blm_types.t_params
    ) return number
        ------------------------------------------------------------------------------------------------------------------------
--  Description:
--    	   This function calculates the maximum number of lost protons for the given enery and loss duration for
--         given protected element type
--
------------------------------------------------------------------------------------------------------------------------
--  Input Arguments:
--   i_protected_element_type - the protected element type
--   i_energy - the energy
--   i_time - loss duration
--   i_params - the required configuration parameters
------------------------------------------------------------------------------------------------------------------------
--  Returns
--    the number of lost protons
------------------------------------------------------------------------------------------------------------------------
--  Exceptions:
--    Raises commons error event BLM_THRESH_UNKNOWN_EL_TYPE when the given protected element type is not known
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--    2014-01-20 : Marcin Sobieszek
--    Creation
------------------------------------------------------------------------------------------------------------------------
        is
    begin
        case i_element_type_rec.parent_type
            when lsa_blm_types.c_warm_magnet then
                return get_wm_max_lost_protons(i_energy, i_time, i_params);
            when lsa_blm_types.c_collimator then
                return get_coll_max_lost_protons(i_energy, i_time, i_params);
            when lsa_blm_types.c_collimator_run3 then
                return get_coll_run3_max_lost_protons(i_energy, i_time, i_params);
            else
                return get_cm_nr_of_protons(i_energy, i_time, i_params, i_element_type_rec.element_type);
            end case;

        com_event_mgr.raise_error_event('BLM_THRESH_UNKNOWN_EL_TYPE', 'unknown protected element type = '
            || i_element_type_rec.parent_type ||dbms_utility.format_error_stack);
    end get_max_lost_protons;

    function get_maximum_thresholds
        return lsa_blm_types.t_thresholds
        ------------------------------------------------------------------------------------------------------------------------
--  Description:
--    	   This function gets thresholds maximum values
--
------------------------------------------------------------------------------------------------------------------------
--  Input Arguments:
------------------------------------------------------------------------------------------------------------------------
--  Returns
--    the collection of thresholds with their maximum values set
------------------------------------------------------------------------------------------------------------------------
--  Exceptions:
--    No known exceptions
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--    2014-01-27 : Marcin Sobieszek
--    Creation
------------------------------------------------------------------------------------------------------------------------
        is
        l_thresholds                lsa_blm_types.t_thresholds := lsa_blm_types.t_thresholds();
        l_threshold_values          lsa_blm_types.t_integration_times;
    begin
        for r_e in 1 .. 32 loop
            l_thresholds.extend;
            l_threshold_values := lsa_blm_types.t_integration_times();
            for r_it in 1 .. 12 loop
                l_threshold_values.extend;
                l_threshold_values(r_it) := lsa_blm_types.c_max_bits(r_it);
            end loop;
            l_thresholds(r_e) := l_threshold_values;
        end loop;

        return l_thresholds;
    end get_maximum_thresholds;

    function calculate_thresholds(
        i_family_name            in blm_families.family_name%type
    ) return lsa_blm_types.t_thresholds
        ------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--    2014-01-15 : Marcin Sobieszek
--    Creation
------------------------------------------------------------------------------------------------------------------------
        is
        l_element_type_rec          lsa_blm_types.t_element_type_rec;
        l_params                    lsa_blm_types.t_params;
        l_thresholds                lsa_blm_types.t_thresholds := lsa_blm_types.t_thresholds();
        l_threshold_values          lsa_blm_types.t_integration_times;
        l_threshold                 number;
        l_rs_time                   number;
        l_nr_of_protons             number;
        l_energy_per_proton         number;
        l_beam_energy               number;
        l_output                    varchar2(4000);
    begin
        com_logger.info('Threshold calculations triggered for family [' || i_family_name || '] ', $$PLSQL_UNIT);
        l_element_type_rec  :=  get_protected_element_type(i_family_name);
        if l_element_type_rec.parent_type = lsa_blm_types.c_special then
            return get_maximum_thresholds;
        end if;
        l_params := get_params(i_family_name);

        if is_param_supported(l_params, lsa_blm_types.c_set_to_maximum) then
            return get_maximum_thresholds;
        end if;

        validate_params(l_params, l_element_type_rec.parent_type);

        for r_e in 1 .. lsa_blm_types.c_energy_levels_size loop
            l_thresholds.extend;
            l_beam_energy := lsa_blm_types.c_beam_energies(r_e);
            l_threshold_values := lsa_blm_types.t_integration_times();
            l_output := '';
            for r_it in 1 .. lsa_blm_types.c_integration_time_size loop
                l_threshold_values.extend;
                l_rs_time := lsa_blm_types.c_integration_times(r_it);
                l_nr_of_protons := get_max_lost_protons(l_element_type_rec, l_beam_energy, l_rs_time, l_params);
                l_energy_per_proton := get_resp_energy_bits(l_params, l_beam_energy, r_it);
                l_threshold := l_energy_per_proton * l_nr_of_protons;
                l_output := l_output || l_threshold || ' ';
                l_threshold_values(r_it) := floor(l_threshold);
            end loop;
            com_logger.debug('Threshold values for energy level = ' || r_e || ': ' || l_output, $$PLSQL_UNIT);
            l_thresholds(r_e) := l_threshold_values;
        end loop;
        --return l_thresholds;
        return apply_corrections_if_needed(l_thresholds, l_params);
    end calculate_thresholds;

    function convert_thresholds(
        i_family_name            in blm_families.family_name%type,
        i_thresholds             in lsa_blm_types.t_thresholds
    ) return lsa_blm_types.t_threshold_recs
        ------------------------------------------------------------------------------------------------------------------------
--  Description:
--    	   This function converts a two dimensional array of thresholds
--         into one dimensional array of threshold records
--
------------------------------------------------------------------------------------------------------------------------
--  Input Arguments:
--   i_family_name - the family name
--   i_thresholds - the two dimensional array of thresholds
------------------------------------------------------------------------------------------------------------------------
--  Returns
--    the collection of threshold records
------------------------------------------------------------------------------------------------------------------------
--  Exceptions:
--    No known exceptions
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--    2014-08-27 : Marcin Sobieszek
--    Creation
------------------------------------------------------------------------------------------------------------------------

        is
        l_thresholds_rec            lsa_blm_types.t_threshold_recs;
    begin
        l_thresholds_rec := lsa_blm_types.t_threshold_recs();
        for energy in 1 .. i_thresholds.last loop
            l_thresholds_rec.extend;
            l_thresholds_rec(energy).family_name         := i_family_name;
            l_thresholds_rec(energy).beam_energy_pos     := energy;
            l_thresholds_rec(energy).time_rs01_threshold := i_thresholds(energy)(1);
            l_thresholds_rec(energy).time_rs02_threshold := i_thresholds(energy)(2);
            l_thresholds_rec(energy).time_rs03_threshold := i_thresholds(energy)(3);
            l_thresholds_rec(energy).time_rs04_threshold := i_thresholds(energy)(4);
            l_thresholds_rec(energy).time_rs05_threshold := i_thresholds(energy)(5);
            l_thresholds_rec(energy).time_rs06_threshold := i_thresholds(energy)(6);
            l_thresholds_rec(energy).time_rs07_threshold := i_thresholds(energy)(7);
            l_thresholds_rec(energy).time_rs08_threshold := i_thresholds(energy)(8);
            l_thresholds_rec(energy).time_rs09_threshold := i_thresholds(energy)(9);
            l_thresholds_rec(energy).time_rs10_threshold := i_thresholds(energy)(10);
            l_thresholds_rec(energy).time_rs11_threshold := i_thresholds(energy)(11);
            l_thresholds_rec(energy).time_rs12_threshold := i_thresholds(energy)(12);
        end loop;
        return l_thresholds_rec;
    end convert_thresholds;

    procedure calculate_and_merge_thresholds(
        i_family_name            in blm_families.family_name%type
    )
        ------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--    2014-08-26 : Marcin Sobieszek
--    Creation
------------------------------------------------------------------------------------------------------------------------
        is
        l_thresholds                lsa_blm_types.t_thresholds;
        l_thresholds_rec            lsa_blm_types.t_threshold_recs;
        l_output                    varchar2(4000);
    begin
        l_thresholds     := calculate_thresholds(i_family_name);
        l_thresholds_rec := convert_thresholds(i_family_name, l_thresholds);
        forall i in l_thresholds_rec.first .. l_thresholds_rec.last
            merge into stage_blm_family_thresholds t1
            using (select l_thresholds_rec(i).family_name family_name,
                          l_thresholds_rec(i).beam_energy_pos beam_energy_pos,
                          l_thresholds_rec(i).time_rs01_threshold time_rs01_threshold,
                          l_thresholds_rec(i).time_rs02_threshold time_rs02_threshold,
                          l_thresholds_rec(i).time_rs03_threshold time_rs03_threshold,
                          l_thresholds_rec(i).time_rs04_threshold time_rs04_threshold,
                          l_thresholds_rec(i).time_rs05_threshold time_rs05_threshold,
                          l_thresholds_rec(i).time_rs06_threshold time_rs06_threshold,
                          l_thresholds_rec(i).time_rs07_threshold time_rs07_threshold,
                          l_thresholds_rec(i).time_rs08_threshold time_rs08_threshold,
                          l_thresholds_rec(i).time_rs09_threshold time_rs09_threshold,
                          l_thresholds_rec(i).time_rs10_threshold time_rs10_threshold,
                          l_thresholds_rec(i).time_rs11_threshold time_rs11_threshold,
                          l_thresholds_rec(i).time_rs12_threshold time_rs12_threshold
                   from dual) t2
            on (t1.family_name = t2.family_name and t1.beam_energy_pos = t2.beam_energy_pos)
            when matched then update set t1.time_rs01_threshold = t2.time_rs01_threshold,
                                         t1.time_rs02_threshold = t2.time_rs02_threshold,
                                         t1.time_rs03_threshold = t2.time_rs03_threshold,
                                         t1.time_rs04_threshold = t2.time_rs04_threshold,
                                         t1.time_rs05_threshold = t2.time_rs05_threshold,
                                         t1.time_rs06_threshold = t2.time_rs06_threshold,
                                         t1.time_rs07_threshold = t2.time_rs07_threshold,
                                         t1.time_rs08_threshold = t2.time_rs08_threshold,
                                         t1.time_rs09_threshold = t2.time_rs09_threshold,
                                         t1.time_rs10_threshold = t2.time_rs10_threshold,
                                         t1.time_rs11_threshold = t2.time_rs11_threshold,
                                         t1.time_rs12_threshold = t2.time_rs12_threshold
            when not matched then insert (family_name, beam_energy_pos,
                                          time_rs01_threshold, time_rs02_threshold, time_rs03_threshold,
                                          time_rs04_threshold, time_rs05_threshold, time_rs06_threshold,
                                          time_rs07_threshold, time_rs08_threshold, time_rs09_threshold,
                                          time_rs10_threshold, time_rs11_threshold, time_rs12_threshold
            )
                                  values (t2.family_name, t2.beam_energy_pos,
                                          t2.time_rs01_threshold, t2.time_rs02_threshold, t2.time_rs03_threshold,
                                          t2.time_rs04_threshold, t2.time_rs05_threshold, t2.time_rs06_threshold,
                                          t2.time_rs07_threshold, t2.time_rs08_threshold, t2.time_rs09_threshold,
                                          t2.time_rs10_threshold, t2.time_rs11_threshold, t2.time_rs12_threshold
                                         );
    end calculate_and_merge_thresholds;

end lsa_blm_threshold_calcs;
