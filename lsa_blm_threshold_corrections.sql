create or replace 
package body lsa_blm_threshold_corrections
is
------------------------------------------------------------------------------------------------------------------------
--  (c) CERN 2005-2014 copyright
------------------------------------------------------------------------------------------------------------------------
--  Description:
--   This package is the entry point for calculating Beam Loss Monitor beam abort thresholds corrections.
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2014-01-14 : Marcin Sobieszek
--    Creation
------------------------------------------------------------------------------------------------------------------------
  procedure apply_scale_correction(
    io_thresholds            in out lsa_blm_types.t_thresholds,
    i_scale_factor           in number
  ) 
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--    2014-01-14 : Marcin Sobieszek
--    Creation
------------------------------------------------------------------------------------------------------------------------
  is
  begin
    com_assert.arg_not_null(i_scale_factor, 'Scale factor');
    
    for r_energy in 1 .. io_thresholds.count loop
      for r_int_time in 1 .. io_thresholds(r_energy).count loop
        io_thresholds(r_energy)(r_int_time) := io_thresholds(r_energy)(r_int_time) * i_scale_factor;
      end loop;
    end loop;
    
  end apply_scale_correction;  

  function get_blm_response_filter_for(
    i_threshold            in number,
    i_integration_time     in number,
    i_time_constant        in number
  ) return number
------------------------------------------------------------------------------------------------------------------------  
--  Description:
--            This function returns the response for the ionization chamber in case an RC delay has been installed 
--            (inisde the signal box). At the moment we have 2 different types of filters, therefore the argument 
--            for this function is as well the time_constant.
--            
------------------------------------------------------------------------------------------------------------------------
--  Input Arguments:
--   io_thresholds      - the calculated thresholds
--   i_integration_time - the integration time
--   i_time_constant    - the tauRC
------------------------------------------------------------------------------------------------------------------------
--  Returns
--    the modified thresholds
------------------------------------------------------------------------------------------------------------------------
--  Exceptions:
--    Raises commons error event BLM_THRESH_C_TIME_C_MISSING when the time constant is null or 0
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--    2014-01-14 : Marcin Sobieszek
--    Creation
------------------------------------------------------------------------------------------------------------------------
  is
    l_factor                  number := 0;
  begin
    l_factor := -1 * i_integration_time / i_time_constant;
    return i_threshold * (1 - exp(l_factor));
  end get_blm_response_filter_for;

  procedure apply_rc_correction(
    io_thresholds            in out lsa_blm_types.t_thresholds,
    i_time_constant          in number
  )
  is
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--    2014-01-14 : Marcin Sobieszek
--    Creation
------------------------------------------------------------------------------------------------------------------------
  begin
    com_assert.arg_not_null(i_time_constant, 'Time constant');
    
    for r_energy in 1 .. io_thresholds.count loop
      for r_int_time in 1 .. io_thresholds(r_energy).count loop
        io_thresholds(r_energy)(r_int_time) := get_blm_response_filter_for(io_thresholds(r_energy)(r_int_time), lsa_blm_types.c_integration_times(r_int_time), i_time_constant);
      end loop;
    end loop;
  end apply_rc_correction;

  procedure apply_il_correction(
    io_thresholds            in out lsa_blm_types.t_thresholds,
    i_time_constant          in number,
    i_blm_conv_bit_2_gy      in number,
    i_norm                   in number
  )
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--    2014-01-14 : Marcin Sobieszek - Creation
--    2015-07-15 : Marcin Sobieszek - BLMDM-119 - Modify IL Correction
------------------------------------------------------------------------------------------------------------------------
  is
    l_norm                      number;
    l_injection_loss            number;
    l_int_time                  number;
  begin
    com_assert.arg_not_null(i_time_constant, 'Time constant');
    if i_time_constant = 0 then
      com_event_mgr.raise_event('BLM_THRESH_C_TIME_C_MISSING', 'The time constant must be defined as non-zero value'
        ||dbms_utility.format_error_stack, true, true); 
    end if;

    l_norm := i_norm * lsa_blm_types.c_integration_times(1) / i_blm_conv_bit_2_gy;
    l_norm := l_norm / (1 - exp(-1 * lsa_blm_types.c_integration_times(1) / i_time_constant));
    for r_energy in 1 .. lsa_blm_types.c_max_energy_level_inj loop
      for r_int_time in 1 .. io_thresholds(r_energy).count loop
        l_int_time := lsa_blm_types.c_integration_times(r_int_time);
        l_injection_loss := l_norm * ( 1 - exp(-1 * l_int_time / i_time_constant));
        io_thresholds(r_energy)(r_int_time) := greatest(l_injection_loss, io_thresholds(r_energy)(r_int_time));
      end loop;
    end loop;
    
  end apply_il_correction;  

  procedure apply_off_bits_correction(
    io_thresholds            in out lsa_blm_types.t_thresholds,
    i_blm_off_bits           in v_stage_blm_threshold_attrs.vector_value%type
  ) 
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--    2014-01-14 : Marcin Sobieszek
--    Creation
------------------------------------------------------------------------------------------------------------------------
  is
    l_value                  number;
  begin
    for r_energy in 1 .. io_thresholds.count loop
      for r_int_time in 1 .. io_thresholds(r_energy).count loop
        l_value := lsa_blm_types.c_off_bits(r_int_time); 
        if i_blm_off_bits.count >= r_int_time then
          l_value := i_blm_off_bits(r_int_time);
        end if;
        io_thresholds(r_energy)(r_int_time) := io_thresholds(r_energy)(r_int_time) + l_value;
      end loop;
    end loop;
  end apply_off_bits_correction;
  
  procedure apply_min_bits_correction(
    io_thresholds            in out lsa_blm_types.t_thresholds,
    i_scale_factor           in number default 1
  ) 
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--    2014-01-14 : Marcin Sobieszek
--    Creation
------------------------------------------------------------------------------------------------------------------------
  is
    l_min_value                 number;
  begin
    for r_energy in 1 .. io_thresholds.count loop
      for r_int_time in 1 .. io_thresholds(r_energy).count loop
        l_min_value := lsa_blm_types.c_min_bits(r_int_time) * coalesce(i_scale_factor, 1); 
        io_thresholds(r_energy)(r_int_time) := greatest(l_min_value, io_thresholds(r_energy)(r_int_time));
      end loop;
    end loop;
  end apply_min_bits_correction;

  procedure apply_max_bits_correction(
    io_thresholds            in out lsa_blm_types.t_thresholds,
    i_max_bits_factor        in number default 1,
    i_max_bits               in v_stage_blm_threshold_attrs.vector_value%type
  )
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--    2014-01-14 : Marcin Sobieszek
--    Creation
------------------------------------------------------------------------------------------------------------------------
  is
    l_max_bits_factor           number;
    l_max_value                 number;
  begin
    l_max_bits_factor := coalesce(i_max_bits_factor, 1);
    
    for r_energy in 1 .. io_thresholds.count loop
      for r_int_time in 1 .. io_thresholds(r_energy).count loop
        if i_max_bits.count = 0 then 
          l_max_value := lsa_blm_types.c_max_bits(r_int_time); 
        else 
          l_max_value := i_max_bits(r_int_time); 
        end if;

        io_thresholds(r_energy)(r_int_time) := io_thresholds(r_energy)(r_int_time) * l_max_bits_factor;
        io_thresholds(r_energy)(r_int_time) := least(l_max_value, io_thresholds(r_energy)(r_int_time)) / l_max_bits_factor;
      end loop;
    end loop;
  end apply_max_bits_correction;  

  procedure apply_decrease_correction(
    io_thresholds            in out lsa_blm_types.t_thresholds,
    i_correct_rsum           in boolean default true,
    i_correct_elevel         in boolean default true
  )
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--    2014-01-14 : Marcin Sobieszek
--    Creation
------------------------------------------------------------------------------------------------------------------------
  is
  begin
    for r_e in 1 .. io_thresholds.count loop
      for r_t in 3 .. io_thresholds(r_e).count loop
      
        if i_correct_rsum then
          if (io_thresholds(r_e)(r_t) / lsa_blm_types.c_integration_times(r_t)) > (io_thresholds(r_e)(r_t - 1) / lsa_blm_types.c_integration_times(r_t - 1)) then
            io_thresholds(r_e)(r_t) := io_thresholds(r_e)(r_t - 1) * floor(lsa_blm_types.c_integration_times(r_t) / lsa_blm_types.c_integration_times(r_t - 1));
          end if;
          io_thresholds(r_e)(r_t) := greatest(io_thresholds(r_e)(r_t), io_thresholds(r_e)(r_t - 1));
        end if;
        
        if i_correct_elevel and r_e > 1 then
          io_thresholds(r_e)(r_t) := least(io_thresholds(r_e)(r_t), io_thresholds(r_e - 1)(r_t));
        end if;
        
      end loop;
    end loop;
  end apply_decrease_correction;  

  procedure apply_adhoc_factor_correction(
    io_thresholds            in out lsa_blm_types.t_thresholds,
    i_beam_level             in v_stage_blm_threshold_attrs.vector_value%type,
    i_scale_rs               in v_stage_blm_threshold_attrs.vector_value%type
  ) 
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--    2014-01-14 : Marcin Sobieszek - Creation
--    2015-05-22 : Marcin Sobieszek - BLMDM-109 - Modification of AD-HOC-(BITS) Correction
------------------------------------------------------------------------------------------------------------------------
  is
    l_factor_rs                 number := 1;
  begin
    for r_t in 1 .. lsa_blm_types.c_integration_time_size loop
      l_factor_rs := i_scale_rs(r_t);
      for r_e in 1 .. lsa_blm_types.c_energy_levels_size loop
        if i_beam_level.count >= r_e and i_beam_level(r_e) = 1 then
          io_thresholds(r_e)(r_t) := io_thresholds(r_e)(r_t) * l_factor_rs;
        end if;
      end loop;
      
    end loop;
  end apply_adhoc_factor_correction;  

  procedure apply_adhoc_fix_rs_correction(
    io_thresholds            in out lsa_blm_types.t_thresholds,
    i_beam_level             in v_stage_blm_threshold_attrs.vector_value%type,
    i_fix_to_rs              in v_stage_blm_threshold_attrs.vector_value%type
  )
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--    2014-01-14 : Marcin Sobieszek - Creation
--    2015-05-22 : Marcin Sobieszek - BLMDM-109 - Modification of AD-HOC-(BITS) Correction
------------------------------------------------------------------------------------------------------------------------
  is
    l_correction                number;
  begin
    for r_t in 1 .. lsa_blm_types.c_integration_time_size loop
      for r_e in 1 .. lsa_blm_types.c_energy_levels_size loop
      
        if i_beam_level.count >= r_e and i_beam_level(r_e) = 1 then
          l_correction := lsa_blm_types.c_integration_times(r_t) / lsa_blm_types.c_integration_times(i_fix_to_rs(r_t));
          io_thresholds(r_e)(r_t) := io_thresholds(r_e)(i_fix_to_rs(r_t)) * l_correction;
        end if;
        
      end loop;
    end loop;
  end apply_adhoc_fix_rs_correction;
  
  procedure apply_adhoc_bits_correction(
    io_thresholds            in out lsa_blm_types.t_thresholds,
    i_beam_level             in v_stage_blm_threshold_attrs.vector_value%type,
    i_fix_bits               in v_stage_blm_threshold_attrs.vector_value%type,
    i_blm_conv_bit_2_gy      in number
  )
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--    2014-01-14 : Marcin Sobieszek - Creation
--    2015-05-22 : Marcin Sobieszek - BLMDM-109 - Modification of AD-HOC-(BITS) Correction
------------------------------------------------------------------------------------------------------------------------
  is
    l_fix_bit                   number;
  begin
    for r_int_time in 1 .. lsa_blm_types.c_integration_time_size loop
      for r_energy in 1 .. lsa_blm_types.c_energy_levels_size loop
      
        if i_beam_level.count >= r_energy and i_beam_level(r_energy) = 1  and i_fix_bits(r_int_time) != -1 then
          l_fix_bit := i_fix_bits(r_int_time);
          io_thresholds(r_energy)(r_int_time) := l_fix_bit * lsa_blm_types.c_integration_times(r_int_time) / i_blm_conv_bit_2_gy;
        end if;
        
      end loop;
    end loop;
  end apply_adhoc_bits_correction;
  
  
end lsa_blm_threshold_corrections;
