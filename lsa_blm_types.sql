create or replace 
package lsa_blm_types
is
------------------------------------------------------------------------------------------------------------------------
--  (c) CERN 2005-2014 copyright
------------------------------------------------------------------------------------------------------------------------
--  Description:
--   This package is intended for defining all PL/SQL types required for use in BLM code
------------------------------------------------------------------------------------------------------------------------
--  Change Log (descending):
--  2014-01-15 : Marcin Sobieszek
--    Creation
------------------------------------------------------------------------------------------------------------------------
  type t_integration_times   is varray(12) of number;
  type t_thresholds          is varray(32) of t_integration_times;  
  type t_threshold_recs      is varray(32) of stage_blm_family_thresholds%rowtype;  
  type t_beam_energies       is varray(32) of number;
  type t_beam_level          is varray(32) of boolean;
  type t_bits                is varray(12) of number;
  type t_corrections         is varray(10) of blm_threshold_operations.operation_name%type ;
  type t_uf_corrections      is varray(5)  of number;

--  element types protected by BLM monitors  
  type t_element_type_rec     is record (
    element_type    blm_protected_elem_types.element_type%type,
    parent_type     blm_protected_elem_types.parent_type%type
  ); 
  
  subtype t_element_type     is blm_protected_elem_types.element_type%type;
  c_warm_magnet              constant  t_element_type :=  'WARM_MAGNET';
  c_cold_magnet              constant  t_element_type :=  'COLD_MAGNET';
  c_collimator               constant  t_element_type :=  'COLLIMATOR';
  c_special                  constant  t_element_type :=  'SPECIAL';
  c_cold_qp3                 constant  t_element_type :=  'COLD_QP3';
  
  subtype t_regime           is varchar2(20);
  c_regime_transient         constant t_regime        := 'TRANSIENT';
  c_regime_intermediate      constant t_regime        := 'INTERMEDIATE';
  c_regime_steady            constant t_regime        := 'STEADY';
  
  c_safety_factor            constant  binary_double  := 3;     -- safety factor: Master thresholds should be 3 times the estimated quench level
  c_ecoll_tev                constant  binary_double  := 7;     -- collision energy
  c_eInjection_tev           constant  binary_double  := 0.45;  -- injection energy
  c_energy_diff              constant  binary_double  := c_ecoll_tev - c_eInjection_tev;
  c_fast_lost_time           constant  binary_double  := 1.0;   -- 1 second
  c_slow_lost_time           constant  binary_double  := 10.0;  -- 10 seconds
  c_mg_he_frac               constant  binary_double  := 0.05;  -- Helium fraction in the cable

  c_t_quench_slope           constant  binary_double  := -0.9465648;
  c_t_quench_offset          constant  binary_double  := 9.425954;
  c_q_he1_slope              constant  binary_double  := 0.8694;
  c_q_he1_offset             constant  binary_double  := -1.6518;
  c_q_he2_slope              constant  binary_double  := 0.9226;
  c_q_he2_offset             constant  binary_double  := -2.003; 
--  c_blm_resp_offset          constant  binary_double  := 0; 
--  c_blm_resp_slope           constant  binary_double  := 1e-18; 
  c_blm_conva_c2             constant  binary_double  := 1;     -- conversion factor from C to aC
  c_t_he_21                  constant  binary_double  := 2.168;
  c_max_energy_level_inj     constant  binary_integer:= 2;
  c_interpolated             constant varchar2(12)    := 'interpolated';
  c_integration_time_size    constant  binary_integer := 12;
  c_energy_levels_size       constant  binary_integer := 32;
  
  
  
-- params holder
  type t_param_rec is record (
    operation_name  blm_threshold_operations.operation_name%type,
    operation_order v_stage_blm_threshold_attrs.action_order%type,
    param_name      blm_threshold_parameters.parameter_name%type,
    param_value     v_stage_blm_threshold_attrs.scalar_value%type,
    param_values    v_stage_blm_threshold_attrs.vector_value%type,
    param_config    v_stage_blm_threshold_attrs.config_value%type
  );
  
  type t_params is table of t_param_rec not null
    index by pls_integer;

-- params
  subtype t_param is blm_threshold_parameters.parameter_name%type;
  c_wm_np_long_form          constant t_param := 'WMAG.NprotLong_FORM';
  c_wm_np_short_form         constant t_param := 'WMAG.NprotShort_FORM';
  c_wm_np_long_param         constant t_param := 'WMAG.NprotLong_PAR';
  c_wm_np_short_param        constant t_param := 'WMAG.NprotShort_PAR';

  c_coll_np_short_inj        constant t_param := 'COLL.NpShort_inj';
  c_coll_dndt_mid_inj        constant t_param := 'COLL.dNdtMid_inj';
  c_coll_dndt_long_inj       constant t_param := 'COLL.dNdtLong_inj';
  c_coll_np_short_coll       constant t_param := 'COLL.NpShort_coll';
  c_coll_dndt_mid_coll       constant t_param := 'COLL.dNdtMid_coll';
  c_coll_dndt_long_coll      constant t_param := 'COLL.dNdtLong_coll';
  c_coll_corr_rs             constant t_param := 'COLL.Corr_RS';
  
  c_blm_corr_rs              constant t_param := 'blmCorrRS';

  c_blm_resp_form            constant t_param := 'BLM.RespFORM';
  c_blm_resp_form_param      constant t_param := 'BLM.RespPAR';

  c_blm_conv_gy_2c           constant t_param := 'blmConvGy2C';
  c_blm_conv_bit_2gy         constant t_param := 'blmConvBit2Gy';
  
  c_set_to_maximum           constant t_param := 'set_to_maximum';
  
-- cold magnets
  c_mg_tau_me_form           constant t_param := 'MG.tauMe_FORM';
  c_mg_tau_me_form_param     constant t_param := 'MG.tauMe_PAR';
  c_mg_tau_he_form           constant t_param := 'MG.tauHe_FORM';
  c_mg_tau_he_form_param     constant t_param := 'MG.tauHe_PAR';
  c_mg_en_li_form            constant t_param := 'MG.EnLi_FORM';
  c_mg_en_li_form_param      constant t_param := 'MG.EnLi_PAR';
  c_mg_en_li_form_points_x   constant t_param := 'MG.EnLi_PT0';
  c_mg_en_li_form_points_y   constant t_param := 'MG.EnLi_PT1';
  c_mg_qu_li_form            constant t_param := 'MG.QuLi_FORM';
  c_mg_qu_li_form_param      constant t_param := 'MG.QuLi_PAR';
  
  c_tau_transient_user       constant t_param := 'CM.Tau_transient_user';
  c_tau_steady_user          constant t_param := 'CM.Tau_steady_user';

  c_loss_edep_max_form       constant t_param := 'LOSS.EdepMax_FORM';
  c_loss_edep_max_form_param constant t_param := 'LOSS.EdepMax_PAR';
  c_loss_edep_max_f_points_x constant t_param := 'LOSS.EdepMax_PT_X_';
  c_loss_edep_max_f_points_y constant t_param := 'LOSS.EdepMax_PT_Y_';
  c_loss_edep_th_form        constant t_param := 'LOSS.EdepThermal_FORM';
  c_loss_edep_th_form_param  constant t_param := 'LOSS.EdepThermal_PAR';
  c_qp3_table_param          constant t_param := 'QP3.Table';
  
  
  
-- params related to corrections
  c_adhoc_corr_factor        constant t_param := 'AD_HOC_FACTOR_CORRECTION';
  c_adhoc_corr_bits          constant t_param := 'AD_HOC_BITS_CORRECTION'; 
  c_adhoc_corr_fix2rs        constant t_param := 'AD_HOC_FIX_TO_RS_CORRECTION';
  c_correct_decrease         constant t_param := 'DECREASE_CORRECTION';
  c_correct_max_bits         constant t_param := 'MAX_BITS_CORRECTION';
  c_correct_min_bits         constant t_param := 'MIN_BITS_CORRECTION';
  c_correct_off_bits         constant t_param := 'OFF_BITS_CORRECTION';
  c_scale_correction         constant t_param := 'SCALE_CORRECTION';  
  c_il_correction            constant t_param := 'IL_CORRECTION';
  c_rc_correction            constant t_param := 'RC_CORRECTION';
  
  c_adhoc_corr_factor_rsum   constant t_param := 'AD_HOC_FACTOR_RSUM';
  c_adhoc_corr_factor_elev   constant t_param := 'AD_HOC_FACTOR_ELEVEL';
  c_adhoc_corr_fix2rs_rsum   constant t_param := 'AD_HOC_FIX_TO_RS_RSUM';
  c_adhoc_corr_fix2rs_elev   constant t_param := 'AD_HOC_FIX_TO_RS_ELEVEL';
  c_adhoc_corr_bits_fix_bits constant t_param := 'AD_HOC_BITS_FIX_BITS'; 
  c_adhoc_corr_bits_elev     constant t_param := 'AD_HOC_BITS_ELEVEL'; 
  c_corr_scale_factor        constant t_param := 'SCALE_FACTOR';
  c_rc_correction_tc         constant t_param := 'RC_TIME_CONSTANT';
  c_il_correction_tc         constant t_param := 'IL_TIME_CONSTANT';
  c_correct_off_bits_param   constant t_param := 'BLM_OFF_BITS';
  c_correct_max_bits_param   constant t_param := 'BLM_MAX_BITS';
  c_correct_min_bits_param   constant t_param := 'BLM_MIN_BITS';
  c_correct_min_bits_f_param constant t_param := 'BLM_MIN_BITS_SCALE_FACTOR';
  c_il_correction_norm       constant t_param := 'IL_NORM';
  c_correct_decrease_rs      constant t_param := 'DECREASE_RSUM';  
  c_correct_decrease_el      constant t_param := 'DECREASE_ELEVEL';
  c_correct_max_bits_fr      constant t_param := 'BLM_MAX_BITS_FACTOR';
  
  c_t_quench_slope_param     constant t_param := 'CM.Tquench_slope';
  c_t_quench_offset_param    constant t_param := 'CM.Tquench_offset';
  
  c_beam_energies            constant t_beam_energies := t_beam_energies (
    0.45,
    0.4914,
    0.73716,
    0.98292,
    1.22868,
    1.474,
    1.7202,
    1.96596,
    2.21172,
    2.45748,
    2.70324,
    2.949,
    3.19476,
    3.44052,
    3.68628,
    3.93204,
    4.1778,
    4.42356,
    4.66932,
    4.91508,
    5.16084,
    5.4066,
    5.65236,
    5.89812,
    6.14388,
    6.38964,
    6.6354,
    6.88116,
    7,
    7,
    7,
    7
  );
  
  c_integration_times        constant t_integration_times := t_integration_times (
    .00004,
    .00008,
    .00032,
    .00064,
    .00256,
    .01024,
    .08192,
    .65536,
    1.31072,
    5.24288,
    20.97152,
    83.88608
  );

  c_correction_rs            constant t_integration_times := t_integration_times (
    .4,
    .7,
    1,
    1,
    1,
    1,
    1,
    1,
    1,
    1,
    1,
    1
  );

  c_max_bits                 constant t_bits := t_bits (
    256000,
    512000,
    2048000,
    4096000,
    16384000,
    65536000,
    524288000,
    4194304000,
    8388608000,
    33554432000,
    134217728000,
    536870912000
  );

  c_min_bits                 constant t_bits := t_bits (
    1105,
    1105,
    1105,
    1105,
    1105,
    1105,
    1105,
    1105,
    1105,
    1105,
    1105,
    1105
  );

  c_off_bits                 constant t_bits := t_bits (
    49,
    49,
    49,
    49,
    58,
    71,
    174,
    990,
    1947,
    7742,
    30820,
    123087
  );
  
  c_corrections              constant t_corrections := t_corrections (
    c_correct_decrease,
    c_correct_max_bits,
    c_correct_min_bits,
    c_correct_off_bits,
    c_adhoc_corr_fix2rs,
    c_adhoc_corr_factor,
    c_adhoc_corr_bits,
    c_il_correction,
    c_rc_correction,
    c_scale_correction
  );
  
  c_adhoc_corrections        constant t_corrections := t_corrections (
    c_adhoc_corr_fix2rs,
    c_adhoc_corr_factor,
    c_adhoc_corr_bits
  );

-- Ultra fast correction values for BLMs protecting TCPs in IR7.
  c_uf_corrections           constant t_uf_corrections := t_uf_corrections (
    0.06,
    0.06,
    0.06,
    1.0,
    1.0
  );

end lsa_blm_types;
